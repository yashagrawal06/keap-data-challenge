# -*- coding: utf-8 -*-
"""
Importing libraries needed to perform data cleaning and preliminary data 
analysis

"""
import pandas as pd
import numpy as np
from src.func import *

#Loading Datasets
data = pd.read_csv('data/original_data.csv', encoding='latin1')

#Modifying Columns names (removing spaces with '_')
data.columns = data.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

#Understanding data
data.describe()
data.shape
data.head(10)
pd.set_option('display.max_columns',26)
list(data)

""" Finding total null values & their percentages """
data.isna().sum()
((data.isna() | data.isnull()).sum() * 100/data.index.size).round(2)

""" Dropping most_recent_campaign column """
data = data.drop(['most_recent_campaign'], axis =1)

""" Checking unique values in Lead ID and dropping if duplicates exist """
data['lead_id'].nunique()
data = data.drop_duplicates(subset = 'lead_id')


""" Checking unique values in Opportunity ID and dropping if duplicates exist"""
data['opportunity_id'].nunique()
data = data.drop_duplicates(subset = 'opportunity_id')


""" Checking datatypes for Columns """
data.dtypes

""" Changing the the datatype of date columns to date """
date_columns = ['lead_created_date' ,'lead_assignment_date', 'lead_engagement_time','prospect_created_date','demo_date','opportunity_created_date','opportunity_closed_date','lost_revenue_date']

for col in date_columns:
    data[col] = data[col].astype('datetime64[ns]')


""" Checking beginning and ending dates for date columns """
for col in date_columns:
    print(data[col].describe())
    
""" Removing records for dates before September 1st 2017 """

rm_leadId = ['00Qf100000Yt7YEEAZ','00Qf100000ZMy1uEAD','00Qf100000YuyUmEAJ','00Qf100000ZNlqcEAD','00Qf100000ZOE9FEAX','00Qf100000Yw00wEAB','00Qf100000Yw0oREAR','00Qf100000ZMhs0EAD','00Qf100000ZNGTaEAP','00Qf100000ZPAuEEAX','00Qf100000ZP1TYEA1','00Qf100000XEhmMEAT','00Qf100000XF2LmEAL']
data = data[~data['lead_id'].isin(rm_leadId)]


#Exporting Clean Datasets to desired folder

""" Commenting this Section as the Processed datasets are in the folder """
data.to_csv(r'cleaned-data\data1.csv')
